﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MyDotNetWebProject.Controllers
{
    public class StudentsController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Adams()
        {
            ViewData["Name"] = "David Adams";
            return View();
        }

        public IActionResult Bennett()
        {
            ViewData["Name"] = "Brian T. Bennett, PhD";
            return View();
        }

        public IActionResult Memon()
        {
            ViewData["Name"] = "Memon Talha";
            return View();
        }

         public IActionResult Burchette()
        {
            ViewData["Name"] = "Daniel Anderson Burchette";
            return View();
        }

        public IActionResult Huber()
        {
            ViewData["Name"] = "Donald Huber";
            return View();
        }

        public IActionResult Ortiz()
        {
            ViewData["Name"] = "Anthony Ortiz";
            return View();
        }

        public IActionResult Barlow()
        {
            ViewData["Name"] = "Shane Barlow";
            return View();
        }

        public IActionResult Ding()
        {
            ViewData["Name"] = "Jia Ding";
            return View();
        }

        public IActionResult Floyd()
        {
            ViewData["Name"] = "Mitchell Floyd";
            return View();
        }

        public IActionResult Scruggs()
        {
            ViewData["Name"] = "Gavin Scruggs";
            return View();
        }

        public IActionResult Waser()
        {
            ViewData["Name"] = "Dylan Waser";
            return View();
        }

		public IActionResult Collins()
		{
			ViewData["Name"] = "Meghan Collins";
			return View();
		}
    }
}
